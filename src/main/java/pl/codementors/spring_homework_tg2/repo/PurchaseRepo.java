package pl.codementors.spring_homework_tg2.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.codementors.spring_homework_tg2.models.Purchase;
import java.util.List;

@Repository
public interface PurchaseRepo extends CrudRepository<Purchase, String> {
    List<Purchase> findAll();
}

package pl.codementors.spring_homework_tg2.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class PurchaseTest {

    private Purchase purchase;
    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @Before
    public void setUp() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
        purchase = new Purchase();
    }

    @After
    public void close() {
        validatorFactory.close();
    }

    @Test
    public void purchaseWithAllRequestedFieldsInRequestedFormatShouldHaveNoViolations(){
        purchase.setItems("TestItem1, TestItem2");
        purchase.setCreation_date("2019");
        purchase.setUser_id("testUser1");

        Set<ConstraintViolation<Purchase>> violations = validator.validate(purchase);

        assertTrue(violations.isEmpty());
    }

    @Test
    public void shouldDetectItemsAreEmpty(){
        purchase.setItems("");
        purchase.setCreation_date("2019");
        purchase.setUser_id("testUser1");

        Set<ConstraintViolation<Purchase>> violations = validator.validate(purchase);

        assertEquals(violations.size(), 1);
        ConstraintViolation<Purchase> violation = violations.iterator().next();
        assertEquals("Items list can't be empty", violation.getMessage());
    }

    @Test
    public void shouldDetectItemsAreNull(){
        purchase.setItems(null);
        purchase.setCreation_date("2019");
        purchase.setUser_id("testUser1");

        Set<ConstraintViolation<Purchase>> violations = validator.validate(purchase);

        assertEquals(violations.size()-1, 1);
        ConstraintViolation<Purchase> violation = violations.iterator().next();
        assertEquals("may not be null", violation.getMessage());
    }

    @Test
    public void shouldDetectUserIdIsEmpty(){
        purchase.setItems("TestItem1, TestItem2");
        purchase.setCreation_date("2019");
        purchase.setUser_id("");

        Set<ConstraintViolation<Purchase>> violations = validator.validate(purchase);

        assertEquals(violations.size(), 1);
        ConstraintViolation<Purchase> violation = violations.iterator().next();
        assertEquals("User id can't be empty", violation.getMessage());
    }

    @Test
    public void shouldDetectUserIdIsNull(){

    }
}
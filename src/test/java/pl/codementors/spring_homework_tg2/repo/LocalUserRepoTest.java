package pl.codementors.spring_homework_tg2.repo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.codementors.spring_homework_tg2.models.LocalUser;

import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LocalUserRepoTest {

    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private LocalUserRepo localUserRepo;
    private LocalUser localUser1, localUser2;

    @Before
    public void setUp() {
        localUserRepo.deleteAll();
        localUser1 = new LocalUser();
        localUser2 = new LocalUser();
    }

    @Test
    public void findAllReturnListOfUsers() {
        localUser1.setUsername("this@testUser1.pl");
        localUser1.setPassword("testPassword1");
        localUser2.setUsername("this@testUser2.pl");
        localUser2.setPassword("testPassword2");

        testEntityManager.persist(localUser1);
        testEntityManager.persist(localUser2);
        testEntityManager.flush();

        assertThat(localUserRepo.findAll().size()).isEqualTo(2);
        assertThat(localUserRepo.findAll().get(0).getUsername()).isEqualTo(localUser1.getUsername());
        assertThat(localUserRepo.findAll().get(1).getUsername()).isEqualTo(localUser2.getUsername());
    }

    @Test
    public void findByUsername() {
        localUser1.setUsername("this@testUser.pl");
        localUser1.setPassword("testPassword");
        testEntityManager.persist(localUser1);
        testEntityManager.flush();

        Optional<LocalUser> found = localUserRepo.findByUsername(localUser1.getUsername());

        assertThat(found.get().getUsername()).isEqualTo(localUser1.getUsername());
    }
}
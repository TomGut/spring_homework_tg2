package pl.codementors.spring_homework_tg2.security;

import org.apache.log4j.Logger;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.codementors.spring_homework_tg2.models.LocalUser;
import pl.codementors.spring_homework_tg2.repo.LocalUserRepo;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Service
public class UserDetailsServ implements UserDetailsService {

    final Logger log = Logger.getLogger(getClass());

    private LocalUserRepo localUserRepo;

    //fields validation messages
    private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private Validator validator = factory.getValidator();

    public UserDetailsServ(LocalUserRepo localUserRepo) {
        this.localUserRepo = localUserRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final Optional<LocalUser> localUser = localUserRepo.findByUsername(username);

        log.info(localUser
                        .get()
                        .getUsername());

        return localUser
                .map(this::mapToUser)
                .orElseThrow(() -> new UsernameNotFoundException("Brak użytkownika o imieniu" + username + " w bazie."));
    }

    private User mapToUser(final LocalUser localUser) {

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        for (ConstraintViolation<LocalUser> violation : violations) {
            log.error(violation.getMessage());
        }

        return new User(
                localUser.getUsername(),
                localUser.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
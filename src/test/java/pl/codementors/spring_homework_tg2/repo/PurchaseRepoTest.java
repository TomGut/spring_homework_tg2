package pl.codementors.spring_homework_tg2.repo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.codementors.spring_homework_tg2.models.Purchase;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PurchaseRepoTest {

    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private PurchaseRepo purchaseRepo;
    private Purchase purchase1, purchase2;

    @Before
    public void setUp() {
        purchaseRepo.deleteAll();
        purchase1 = new Purchase();
        purchase2 = new Purchase();
    }

    @Test
    public void findAllReturnsListOfUsers() {
        purchase1.setItems("testItem1, testItem2");
        purchase1.setUser_id("testUser1");
        purchase1.setCreation_date("2018");

        testEntityManager.persistAndFlush(purchase1);

        assertThat(purchaseRepo.findAll().size()).isEqualTo(1);
    }
}
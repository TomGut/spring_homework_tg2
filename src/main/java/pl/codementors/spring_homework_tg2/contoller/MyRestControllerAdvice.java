package pl.codementors.spring_homework_tg2.contoller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.codementors.spring_homework_tg2.models.ApiError;

@ControllerAdvice(assignableTypes = MyRestController.class)
public class MyRestControllerAdvice {
    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ApiError handleConstraintViolation(final DataIntegrityViolationException exception) {
        return new ApiError(exception.getCause().getLocalizedMessage(), exception.getCause().toString());
    }
}

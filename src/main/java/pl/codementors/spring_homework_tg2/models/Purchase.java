package pl.codementors.spring_homework_tg2.models;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Purchase {

    @Id
    @GeneratedValue
    private String id;

    @Column
    @NotNull
    @NotEmpty(message = "Items list can't be empty")
    private String items;

    @Column
    private String creation_date;

    @Column
    @NotNull
    @NotEmpty(message = "User id can't be empty")
    private String user_id;

    public Purchase(){

    }

    public Purchase(String id, String items, String creation_date, String user_id) {
        this.id = id;
        this.items = items;
        this.creation_date = creation_date;
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}

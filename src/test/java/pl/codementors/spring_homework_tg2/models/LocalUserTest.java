package pl.codementors.spring_homework_tg2.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class LocalUserTest {

    private LocalUser localUser;
    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @Before
    public void setUp() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
        localUser = new LocalUser();
    }

    @After
    public void close() {
        validatorFactory.close();
    }

    @Test
    public void userWithAllRequestedFieldsInRequestedFormatShouldHaveNoViolations(){
        localUser.setUsername("this@testUser1.pl");
        localUser.setPassword("testPassword1");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertTrue(violations.isEmpty());
    }

    @Test
    public void shouldDetectUsernameIsNotEmailFormat(){
        localUser.setUsername("testUser1.pl");
        localUser.setPassword("testPassword1");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertEquals(violations.size(), 1);
        ConstraintViolation<LocalUser> violation = violations.iterator().next();
        assertEquals("only email accepted as username", violation.getMessage());
    }

    @Test
    public void shouldDetectUsernameIsEmpty(){
        localUser.setUsername("");
        localUser.setPassword("testPassword1");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertEquals(violations.size(), 1);
        ConstraintViolation<LocalUser> violation = violations.iterator().next();
        assertEquals("username can't be empty", violation.getMessage());
    }

    @Test
    public void shouldDetectUsernameIsNull(){
        localUser.setUsername(null);
        localUser.setPassword("testPassword1");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertEquals(violations.size()-1, 1);
        ConstraintViolation<LocalUser> violation = violations.iterator().next();
        assertEquals("may not be null", violation.getMessage());
    }

    @Test
    public void shouldDetectPasswordIsTooShort(){
        localUser.setUsername("this@testUser1.pl");
        localUser.setPassword("12345");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertEquals(violations.size(), 1);
        ConstraintViolation<LocalUser> violation = violations.iterator().next();
        assertEquals("password must be at least of 8 characters", violation.getMessage());
    }

    @Test
    public void shouldDetectPasswordIsEmpty(){
        localUser.setUsername("this@testUser1.pl");
        localUser.setPassword("");

        Set<ConstraintViolation<LocalUser>> violations = validator.validate(localUser);

        assertEquals(violations.size()-1, 1);
        ConstraintViolation<LocalUser> violation = violations.iterator().next();
        assertEquals("password can't be empty", violation.getMessage());
    }
}
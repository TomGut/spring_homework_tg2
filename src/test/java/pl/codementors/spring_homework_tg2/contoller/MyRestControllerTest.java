package pl.codementors.spring_homework_tg2.contoller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.codementors.spring_homework_tg2.models.LocalUser;
import pl.codementors.spring_homework_tg2.repo.LocalUserRepo;
import pl.codementors.spring_homework_tg2.repo.PurchaseRepo;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.isNull;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyRestControllerTest {

    @MockBean
    private LocalUserRepo localUserRepo;
    @MockBean
    private PurchaseRepo purchaseRepo;
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;
    private MvcResult mvcResult;
    private LocalUser localUser;

    @Before
    public void setup() {
        localUser = new LocalUser();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "this@testUser", password = "testPassword", authorities = "ROLE_USER")
    public void testUserIsAuthenticated() throws Exception {
        assertTrue(SecurityContextHolder
                .getContext()
                .getAuthentication()
                .isAuthenticated());
    }

    @Test
    @WithMockUser(username = "this@testUser", password = "testPassword", authorities = "ROLE_USER")
    public void shouldReturnListOfUsers() throws Exception {
        given(this.localUserRepo.save(localUser)).willReturn(localUser);

        mvcResult = mockMvc.perform(get("/user/list")).andReturn();
        int status = mvcResult.getResponse().getStatus();

        assertEquals(200, status);
        System.out.println("DRUKUJE" + localUserRepo.findAll().size());
    }
}
package pl.codementors.spring_homework_tg2.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.codementors.spring_homework_tg2.models.LocalUser;
import pl.codementors.spring_homework_tg2.models.Purchase;
import pl.codementors.spring_homework_tg2.repo.PurchaseRepo;
import pl.codementors.spring_homework_tg2.repo.LocalUserRepo;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MyRestController {

    @Autowired
    PurchaseRepo purchaseRepo;
    @Autowired
    LocalUserRepo localUserRepo;

    //adding user with validation
    @RequestMapping(value = "user/add", method = RequestMethod.PUT)
    public LocalUser addUser(@Valid @RequestBody LocalUser sentUser){
        LocalUser newUser = new LocalUser();
        newUser.setUsername(sentUser.getUsername());
        newUser.setPassword(sentUser.getPassword());
        localUserRepo.save(newUser);
        return newUser;
    }

    //list of all users
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public List<LocalUser> getAllUsers(){
        List<LocalUser> users = getLocalUsers();
        return users;
    }

    //list of all purchase
    @RequestMapping(value = "/purchase/list", method = RequestMethod.GET)
    public List<Purchase> getAllPurchase(){
        List<Purchase> purchase = getPurchases();
        return purchase;
    }

    //shows current user id
    @RequestMapping(value = "/userId", method = RequestMethod.GET)
    @ResponseBody
    public String currentUserName(Principal principal) {

        String currentUser = getPrincipalName(principal);
        List<LocalUser> users = getLocalUsers();

        return users.stream()
                .filter(localUser ->
                        isEqualToCurrentUser(currentUser, localUser))
                .findFirst()
                .get()
                .getId();
    }

    //shows purchase for current user only
    @RequestMapping(value = "/user-{id}/purchase", method = RequestMethod.GET)
    public List<Purchase> getUserPurchase(@PathVariable("id") String userId,
                                          Principal principal) {

        String currentUser = getPrincipalName(principal);
        List<Purchase> allPurchase;

        if(isEqualToCurrentUser(currentUser, localUserRepo.findOne(userId))){
            allPurchase = purchaseRepo
                    .findAll()
                    .stream()
                    .filter(purchase -> purchase
                            .getUser_id().equals(userId))
                    .collect(Collectors.toList());
        }else throw new DataIntegrityViolationException("nie można podglądać zakupów innego użytkownika");
        return allPurchase;
    }

    //shows purchase for current user only
    @RequestMapping(value = "/user-{id}/purchase-{key}", method = RequestMethod.GET)
    public List<Purchase> getUserPurchaseByAnyName(@PathVariable("id") String userId,
                                                   @PathVariable("key") String anyKey,
                                                   Principal principal) {

        String currentUser = getPrincipalName(principal);
        List<Purchase> allPurchase;

        if(isEqualToCurrentUser(currentUser, localUserRepo.findOne(userId))){
            allPurchase = purchaseRepo
                    .findAll()
                    .stream()
                    .filter(testPurchase ->
                            testPurchase
                                    .getItems()
                                    .contains(anyKey))
                    .collect(Collectors.toList());
        }else throw new DataIntegrityViolationException("nie można pokazać zakupów innego użytkownika");
        return allPurchase;
    }

    //add purchase for current user only - purchase is validated
    @RequestMapping(value = "/user-{id}/addPurchase", method = RequestMethod.PUT)
    public Purchase addPurchase(@PathVariable("id") String  userId,
                                @Valid @RequestBody Purchase userPurchase,
                                Principal principal) {

        Purchase purchase = new Purchase();
        String currentUser = getPrincipalName(principal);

        if(isEqualToCurrentUser(currentUser, localUserRepo.findOne(userId))
                && isEqualToCurrentUser(currentUser, localUserRepo.findOne(userPurchase.getUser_id()))){
            purchase.setItems(userPurchase.getItems());
            purchase.setCreation_date(userPurchase.getCreation_date());
            purchase.setUser_id(userPurchase.getUser_id());
            purchaseRepo.save(purchase);
        }else throw new DataIntegrityViolationException("Nie można dodać zakupów innemu użytkownikowi");
        return purchase;
    }

    //delete purchase for current user only
    @RequestMapping(value = "/user-{userId}/deletePurchase-{purchaseId}", method = RequestMethod.DELETE)
    public Purchase deletePurchase(@PathVariable("userId") String  userId,
                                   @PathVariable("purchaseId") String purchaseId,
                                   Principal principal) {

        String currentUser = getPrincipalName(principal);
        Purchase purchase;

        if(currentUser.equals(
                localUserRepo
                        .findOne(userId)
                        .getUsername())
                && localUserRepo
                        .findOne(userId)
                        .getId()
                        .equals(purchaseRepo.findOne(purchaseId).getUser_id())){
            purchase = purchaseRepo.findOne(purchaseId);
            purchaseRepo.delete(purchase.getId());
        }else throw new DataIntegrityViolationException("Nie można usunąć zakupów innemu użytkownikowi");
        return purchase;
    }

    //update purchase for current user only - purchase is validated
    @RequestMapping(value = "/user-{userId}/updatePurchase-{purchaseId}", method = RequestMethod.POST)
    public Purchase updatePurchase(@PathVariable("userId") String  userId,
                                   @PathVariable("purchaseId") String purchaseId,
                                   @Valid @RequestBody Purchase userPurchase,
                                   Principal principal) {

        String currentUser = getPrincipalName(principal);
        Purchase purchase;

        if(currentUser.equals(
                localUserRepo
                        .findOne(userId)
                        .getUsername())
                && localUserRepo
                .findOne(userId)
                .getId()
                .equals(purchaseRepo.findOne(purchaseId).getUser_id())){
            purchase = purchaseRepo.findOne(purchaseId);
            purchase.setCreation_date(userPurchase.getCreation_date());
            purchase.setItems(userPurchase.getItems());
            purchase.setUser_id(userPurchase.getUser_id());
            purchaseRepo.save(purchase);
        }else throw new DataIntegrityViolationException("Nie można zmienić zakupów innemu użytkownikowi");
        return purchase;
    }

    //refactored to methods below

    //for conditional statement in check is current user access rights
    private boolean isEqualToCurrentUser(String currentUser, LocalUser one) {
        return currentUser.equals(one.getUsername());
    }

    //all users list
    private List<LocalUser> getLocalUsers() {
        return localUserRepo.findAll();
    }

    //principal name
    private String getPrincipalName(Principal principal) {
        return principal.getName();
    }

    //purchase list
    private List<Purchase> getPurchases() {
        return purchaseRepo.findAll();
    }
}
package pl.codementors.spring_homework_tg2.models;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user")
public class LocalUser {

    @Id
    @GeneratedValue
    private String id;

    @Column
    @NotNull
    @NotEmpty(message = "username can't be empty")
    @Email(message = "only email accepted as username")
    private String username;

    @Column
    @NotNull
    @NotEmpty(message = "password can't be empty")
    @Size(min = 8, message = "password must be at least of 8 characters")
    private String password;

    public LocalUser(){

    }

    public LocalUser(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

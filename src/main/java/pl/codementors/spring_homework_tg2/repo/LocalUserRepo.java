package pl.codementors.spring_homework_tg2.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.codementors.spring_homework_tg2.models.LocalUser;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocalUserRepo extends CrudRepository<LocalUser, String> {
    List<LocalUser> findAll();
    Optional<LocalUser> findByUsername(String username);
}
